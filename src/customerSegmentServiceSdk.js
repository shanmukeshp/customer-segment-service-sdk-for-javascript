import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import DiContainer from './diContainer';
import Level1CustomerSegmentView from './level1CustomerSegmentView';
import Level2CustomerSegmentView from './level2CustomerSegmentView';
import Level3CustomerSegmentView from './level3CustomerSegmentView';
import GetCustomerSegmentWithIdFeature from './getCustomerSegmentWithIdFeature';
import ListLevel1CustomerSegmentsFeature from './listLevel1CustomerSegmentsFeature';
import ListLevel2CustomerSegmentsFeature from './listLevel2CustomerSegmentsWithTypeFeature';
import ListLevel3CustomerSegmentsFeature from './listLevel3CustomerSegmentsWithSubTypeFeature';
import CustomerSegmentView from './customerSegmentView';

/**
 * @class {CustomerSegmentServiceSdk}
 */
export default class CustomerSegmentServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {CustomerSegmentServiceSdkConfig} config
     */
    constructor(config:CustomerSegmentServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    getCustomerSegmentWithId(customerSegmentId:number,accessToken:string):Promise<CustomerSegmentView>{
        return this.
            _diContainer
            .get(GetCustomerSegmentWithIdFeature)
            .execute(customerSegmentId,accessToken);
    }

    getLevel1CustomerSegment(accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel1CustomerSegmentsFeature)
            .execute(accessToken);

    }

    getLevel2CustomerSegmentWithType(customerTypeId:number,accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel2CustomerSegmentsFeature)
            .execute(customerTypeId,accessToken);
    }

    getLevel3CustomerSegmentWithSubType(customerSubTypeId:number,accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListLevel3CustomerSegmentsFeature)
            .execute(customerSubTypeId,accessToken);

    }

}
