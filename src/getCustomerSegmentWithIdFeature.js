import {inject} from 'aurelia-dependency-injection';
import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import CustomerSegmentView from './customerSegmentView';

@inject(CustomerSegmentServiceSdkConfig, HttpClient)
class GetCustomerSegmentWithIdFeature {

    _config:CustomerSegmentServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:CustomerSegmentServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists customer segments with Id
     * @param {string} accessToken
     * @returns {Promise.<CustomerSegmentView>}
     */
    execute(customerSegmentWithId:number,accessToken:string):Promise<Array> {

            return this._httpClient
                .createRequest(`/customer-segments/${customerSegmentWithId}`)
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                    // cache
                    //this._cachedLevel1CustomerSegments =
                    return Array.from(
                            response.content,
                            (contentItem) =>
                                new CustomerSegmentView(
                                    contentItem.id,
                                    contentItem.name
                                )
                        );

                    //return this._cachedLevel1CustomerSegments;

                });
        }
}

export default GetCustomerSegmentWithIdFeature;
