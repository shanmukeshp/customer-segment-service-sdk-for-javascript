import {inject} from 'aurelia-dependency-injection';
import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import Level2CustomerSegmentView from './level2CustomerSegmentView';
import Level2CustomerSegmentViewFactory from './level2CustomerSegmentViewFactory';

@inject(CustomerSegmentServiceSdkConfig, HttpClient)
class ListLevel2CustomerSegmentsWithTypeFeature {

    _config:CustomerSegmentServiceSdkConfig;

    _httpClient:HttpClient;

    //_cachedLevel2CustomerSegments:Array<Level2CustomerSegmentView>;

    constructor(config:CustomerSegmentServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all level2 customer segments
     * @param {string} accessToken
     * @returns {Promise.<Level2CustomerSegmentView[]>}
     */
    execute(customerTypeId:number,accessToken:string):Promise<Array> {

        /*if (this._cachedLevel2CustomerSegments) {

            return Promise.resolve(this._cachedLevel2CustomerSegments);

        }
        else {*/
            return this._httpClient
                .createRequest(`/customer-segments/customersubtypes/${customerTypeId}`)
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                        // cache
                        //this._cachedLevel2CustomerSegments =
                        return  Array.from(
                                response.content,
                                (contentItem) =>
                                    Level2CustomerSegmentViewFactory.construct(contentItem)
                            );

                        //return this._cachedLevel2CustomerSegments;
                    }
                );
        //}
    }
}

export default ListLevel2CustomerSegmentsWithTypeFeature;
