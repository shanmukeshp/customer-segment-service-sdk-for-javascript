import {inject} from 'aurelia-dependency-injection';
import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import Level1CustomerSegmentView from './level1CustomerSegmentView';

@inject(CustomerSegmentServiceSdkConfig, HttpClient)
class ListLevel1CustomerSegmentsFeature {

    _config:CustomerSegmentServiceSdkConfig;

    _httpClient:HttpClient;

    //_cachedLevel1CustomerSegments:Array<Level1CustomerSegmentView>;

    constructor(config:CustomerSegmentServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all level1 customer segments
     * @param {string} accessToken
     * @returns {Promise.<Level1CustomerSegmentView[]>}
     */
    execute(accessToken:string):Promise<Array> {

        /*if (this._cachedLevel1CustomerSegments) {

            return Promise.resolve(this._cachedLevel1CustomerSegments);

        }
        else {*/
            return this._httpClient
                .createRequest('/customer-segments/customertypes')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                    // cache
                    //this._cachedLevel1CustomerSegments =
                    return Array.from(
                            response.content,
                            (contentItem) =>
                                new Level1CustomerSegmentView(
                                    contentItem.id,
                                    contentItem.name
                                )
                        );

                    //return this._cachedLevel1CustomerSegments;

                });
        //}
    }
}

export default ListLevel1CustomerSegmentsFeature;
